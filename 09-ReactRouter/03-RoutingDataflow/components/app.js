function App() {

  const [firstMessage] = useState('Message 1');
  const [secondMessage] = useState('Message 2');

  function notify(message) {
    alert('Notification from ' + message)
  }

  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Child 1</Link>
          </li>
          <li>
            <Link to="/child2">Child 2</Link>
          </li>
        </ul>

        <hr/>

        <Switch>
          <Route path="/child2/:id?">
            <Child2 message={secondMessage} notify={notify} />
          </Route>
          <Route exact path="/">
            <Child1 message={firstMessage} notify={notify} />
          </Route>
        </Switch>
      </div>
    </Router>
  );


}
