function WrapperComponent(props) {
  return (
    <div>
      <hr/>
      <div className="container">
        {props.children()}
      </div>
      <hr/>
    </div>
  )
}

function Content() {
  return (
    <div>Test!!</div>
  )
}

function App() {
  return (
    <WrapperComponent>
      {() => {
        return <Content/>
      }}
    </WrapperComponent>
  );
}

ReactDOM.render(<App/>, document.getElementById("root"));

